package com.binary_studio.dependency_detector;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public final class DependencyDetector {

	private DependencyDetector() {
	}

	public static boolean canBuild(DependencyList libraries) {
		boolean canBuild = true;

		Set<String> allLibraries = new HashSet<>(libraries.libraries);
		Map<String, Set<String>> allDependencies = new HashMap<>();

		for (String[] dependency : libraries.dependencies) {
			allDependencies.computeIfAbsent(dependency[0], x -> new HashSet<>()).add(dependency[1]);
		}

		for (String library : allLibraries) {
			Set<String> libraryDependencies = allDependencies.getOrDefault(library, Collections.emptySet());

			Set<String> requestedLibraries = new HashSet<>();

			for (String libraryDependency : libraryDependencies) {
				boolean libraryDependencyCyclic = checkCyclic(libraryDependency, requestedLibraries, allDependencies);
				if (libraryDependencyCyclic) {
					canBuild = false;
					break;
				}
			}
		}

		return canBuild;
	}

	protected static boolean checkCyclic(String targetLibrary, Set<String> requestedLibraries,
			Map<String, Set<String>> allDependencies) {
		boolean dependencyCyclic = false;

		if (requestedLibraries.contains(targetLibrary)) {
			dependencyCyclic = true;
		}
		else {
			requestedLibraries.add(targetLibrary);

			Set<String> subDependencies = allDependencies.getOrDefault(targetLibrary, Collections.emptySet());

			for (String subDependency : subDependencies) {
				boolean subDependencyCyclic = checkCyclic(subDependency, requestedLibraries, allDependencies);
				if (subDependencyCyclic) {
					dependencyCyclic = true;
					break;
				}
			}
		}

		return dependencyCyclic;
	}

}
