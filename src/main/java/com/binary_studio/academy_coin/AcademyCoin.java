package com.binary_studio.academy_coin;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Stream;

public final class AcademyCoin {

	private AcademyCoin() {
	}

	public static int maxProfit(Stream<Integer> prices) {
		AtomicInteger previousPriceAtomic = new AtomicInteger(-1);
		AtomicInteger maxProfitAtomic = new AtomicInteger(0);

		prices.forEach(price -> {
			int previousPrice = previousPriceAtomic.get();
			if (previousPrice >= 0 && price > previousPrice) {
				maxProfitAtomic.addAndGet(price - previousPrice);
			}
			previousPriceAtomic.set(price);
		});

		return maxProfitAtomic.get();
	}

}
