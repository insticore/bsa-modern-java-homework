package com.binary_studio.fleet_commander.core.subsystems;

import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;

public final class AttackSubsystemImpl implements AttackSubsystem {

	private String name;

	private PositiveInteger powerGridConsumption;

	private PositiveInteger capacitorConsumption;

	private PositiveInteger optimalSpeed;

	private PositiveInteger optimalSize;

	private PositiveInteger baseDamage;

	private AttackSubsystemImpl(String name, PositiveInteger powergridRequirments, PositiveInteger capacitorConsumption,
			PositiveInteger optimalSpeed, PositiveInteger optimalSize, PositiveInteger baseDamage) {
		this.name = name;
		this.powerGridConsumption = powergridRequirments;
		this.capacitorConsumption = capacitorConsumption;
		this.optimalSpeed = optimalSpeed;
		this.optimalSize = optimalSize;
		this.baseDamage = baseDamage;

	}

	public static AttackSubsystemImpl construct(String name, PositiveInteger powergridRequirments,
			PositiveInteger capacitorConsumption, PositiveInteger optimalSpeed, PositiveInteger optimalSize,
			PositiveInteger baseDamage) throws IllegalArgumentException {
		if (name == null || name.isEmpty() || name.isBlank()) {
			throw new IllegalArgumentException("Name should be not null and not empty");
		}

		return new AttackSubsystemImpl(name, powergridRequirments, capacitorConsumption, optimalSpeed, optimalSize,
				baseDamage);
	}

	@Override
	public PositiveInteger getPowerGridConsumption() {
		return this.powerGridConsumption;
	}

	@Override
	public PositiveInteger getCapacitorConsumption() {
		return this.capacitorConsumption;
	}

	@Override
	public PositiveInteger attack(Attackable target) {
		int targetSizeValue = target.getSize().value();
		int targetSpeedValue = target.getCurrentSpeed().value();
		int optimalSizeValue = this.optimalSize.value();
		int optimalSpeedValue = this.optimalSpeed.value();

		double sizeReductionModifier = targetSizeValue >= optimalSizeValue ? 1.0
				: (double) targetSizeValue / optimalSizeValue;
		double speedReductionModifier = targetSpeedValue <= optimalSpeedValue ? 1.0
				: optimalSpeedValue / (2.0 * targetSpeedValue);

		int baseDamageValue = this.baseDamage.value();

		double damage = baseDamageValue * Math.min(sizeReductionModifier, speedReductionModifier);
		int finalDamage = (int) Math.ceil(damage);
		return PositiveInteger.of(finalDamage);
	}

	@Override
	public String getName() {
		return this.name;
	}

}
