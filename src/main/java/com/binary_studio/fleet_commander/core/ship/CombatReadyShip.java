package com.binary_studio.fleet_commander.core.ship;

import java.util.Optional;

import com.binary_studio.fleet_commander.core.actions.attack.AttackAction;
import com.binary_studio.fleet_commander.core.actions.defence.AttackResult;
import com.binary_studio.fleet_commander.core.actions.defence.RegenerateAction;
import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.ship.contract.CombatReadyVessel;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class CombatReadyShip implements CombatReadyVessel {

	private String name;

	private PositiveInteger shieldHP;

	private PositiveInteger initShieldHP;

	private PositiveInteger hullHP;

	private PositiveInteger initHullHP;

	private PositiveInteger powergridOutput;

	private PositiveInteger capacitorAmount;

	private PositiveInteger initCapacitorAmount;

	private PositiveInteger capacitorRechargeRate;

	private PositiveInteger speed;

	private PositiveInteger size;

	private AttackSubsystem attackSubsystem;

	private DefenciveSubsystem defenciveSubsystem;

	public CombatReadyShip(String name, PositiveInteger shieldHP, PositiveInteger hullHP,
			PositiveInteger powergridOutput, PositiveInteger capacitorAmount, PositiveInteger capacitorRechargeRate,
			PositiveInteger speed, PositiveInteger size, AttackSubsystem attackSubsystem,
			DefenciveSubsystem defenciveSubsystem) {
		this.name = name;
		this.shieldHP = shieldHP;
		this.initShieldHP = shieldHP;
		this.hullHP = hullHP;
		this.initHullHP = hullHP;
		this.powergridOutput = powergridOutput;
		this.capacitorAmount = capacitorAmount;
		this.initCapacitorAmount = capacitorAmount;
		this.capacitorRechargeRate = capacitorRechargeRate;
		this.speed = speed;
		this.size = size;
		this.attackSubsystem = attackSubsystem;
		this.defenciveSubsystem = defenciveSubsystem;
	}

	@Override
	public void endTurn() {
		refillCapacitorAmount();
	}

	public void refillCapacitorAmount() {
		int initCapacitorAmountValue = this.initCapacitorAmount.value();
		int capacitorAmountValue = this.capacitorAmount.value();
		int capacitorRechargeRateValue = this.capacitorRechargeRate.value();

		if (capacitorAmountValue + capacitorRechargeRateValue > initCapacitorAmountValue) {
			this.capacitorAmount = PositiveInteger.of(initCapacitorAmountValue);
		}
		else {
			this.capacitorAmount = PositiveInteger.of(capacitorAmountValue + capacitorRechargeRateValue);
		}
	}

	@Override
	public void startTurn() {
		// Behaviour of this method is not described in specification, so it does nothing
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public PositiveInteger getSize() {
		return this.size;
	}

	@Override
	public PositiveInteger getCurrentSpeed() {
		return this.speed;
	}

	@Override
	public Optional<AttackAction> attack(Attackable target) {
		Optional<AttackAction> attackResult;

		PositiveInteger capacitorConsumption = this.attackSubsystem.getCapacitorConsumption();

		if (hasCapacity(capacitorConsumption)) {
			reduceCapacitorAmount(capacitorConsumption);
			PositiveInteger damage = this.attackSubsystem.attack(target);
			attackResult = Optional.of(new AttackAction(damage, this, target, this.attackSubsystem));
		}
		else {
			attackResult = Optional.empty();
		}

		return attackResult;
	}

	@Override
	public AttackResult applyAttack(AttackAction attack) {
		AttackResult attackResult = null;

		int damageValue;

		PositiveInteger capacitorConsumption = this.defenciveSubsystem.getCapacitorConsumption();

		if (hasCapacity(capacitorConsumption)) {
			reduceCapacitorAmount(capacitorConsumption);
			damageValue = this.defenciveSubsystem.reduceDamage(attack).damage.value();
		}
		else {
			damageValue = attack.damage.value();
		}

		int shieldHPDiff = this.shieldHP.value() - damageValue;
		int finalShieldHPValue = Math.max(shieldHPDiff, 0);
		this.shieldHP = PositiveInteger.of(finalShieldHPValue);

		if (shieldHPDiff < 0) {
			int hullHPDiff = this.hullHP.value() + shieldHPDiff;
			int finalHullHPValue = Math.max(hullHPDiff, 0);
			this.hullHP = PositiveInteger.of(finalHullHPValue);

			if (finalHullHPValue == 0) {
				attackResult = new AttackResult.Destroyed();
			}
		}

		if (attackResult == null) {
			attackResult = new AttackResult.DamageRecived(attack.weapon, PositiveInteger.of(damageValue),
					attack.target);
		}

		return attackResult;
	}

	@Override
	public Optional<RegenerateAction> regenerate() {
		Optional<RegenerateAction> result;

		PositiveInteger capacitorConsumption = this.defenciveSubsystem.getCapacitorConsumption();

		if (hasCapacity(capacitorConsumption)) {
			RegenerateAction defenciveRegenerateAction = this.defenciveSubsystem.regenerate();

			PositiveInteger shieldHPRegenerated = regenerateShield(defenciveRegenerateAction);
			PositiveInteger hullHPRegenerated = regenerateHull(defenciveRegenerateAction);

			if (shieldHPRegenerated.value() != 0 || hullHPRegenerated.value() != 0) {
				reduceCapacitorAmount(capacitorConsumption);
			}

			result = Optional.of(new RegenerateAction(shieldHPRegenerated, hullHPRegenerated));
		}
		else {
			result = Optional.empty();
		}

		return result;
	}

	protected PositiveInteger regenerateShield(RegenerateAction defenciveRegenerateAction) {

		int initShieldHPValue = this.initShieldHP.value();

		int shieldHPValue = this.shieldHP.value();

		int shieldHPRegenerationValue = defenciveRegenerateAction.shieldHPRegenerated.value();

		int shieldHPRegeneratedValue;

		if (shieldHPValue + shieldHPRegenerationValue > initShieldHPValue) {
			this.shieldHP = PositiveInteger.of(initShieldHPValue);
			shieldHPRegeneratedValue = initShieldHPValue - shieldHPValue;
		}
		else {
			this.shieldHP = PositiveInteger.of(shieldHPValue + shieldHPRegenerationValue);
			shieldHPRegeneratedValue = shieldHPRegenerationValue;
		}

		return PositiveInteger.of(shieldHPRegeneratedValue);
	}

	protected PositiveInteger regenerateHull(RegenerateAction defenciveRegenerateAction) {

		int initHullHPValue = this.initHullHP.value();

		int hullHPValue = this.hullHP.value();

		int hullHPRegenerationValue = defenciveRegenerateAction.hullHPRegenerated.value();

		int hullHPRegeneratedValue;

		if (hullHPValue + hullHPRegenerationValue > initHullHPValue) {
			this.hullHP = PositiveInteger.of(initHullHPValue);
			hullHPRegeneratedValue = initHullHPValue - hullHPValue;
		}
		else {
			this.hullHP = PositiveInteger.of(hullHPValue + hullHPRegenerationValue);
			hullHPRegeneratedValue = hullHPRegenerationValue;
		}

		return PositiveInteger.of(hullHPRegeneratedValue);
	}

	protected boolean hasCapacity(PositiveInteger capacitorConsumption) {
		int capacitorAmountValue = this.capacitorAmount.value();
		int capacitorConsumptionValue = capacitorConsumption.value();

		return capacitorAmountValue >= capacitorConsumptionValue;
	}

	protected void reduceCapacitorAmount(PositiveInteger capacitorConsumption) {
		int capacitorAmountValue = this.capacitorAmount.value();
		int capacitorConsumptionValue = capacitorConsumption.value();

		this.capacitorAmount = PositiveInteger.of(capacitorAmountValue - capacitorConsumptionValue);
	}

}
