package com.binary_studio.fleet_commander.core.ship;

import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.exceptions.InsufficientPowergridException;
import com.binary_studio.fleet_commander.core.exceptions.NotAllSubsystemsFitted;
import com.binary_studio.fleet_commander.core.ship.contract.ModularVessel;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class DockedShip implements ModularVessel {

	private String name;

	private PositiveInteger shieldHP;

	private PositiveInteger hullHP;

	private PositiveInteger powergridOutput;

	private PositiveInteger capacitorAmount;

	private PositiveInteger capacitorRechargeRate;

	private PositiveInteger speed;

	private PositiveInteger size;

	private AttackSubsystem attackSubsystem;

	private DefenciveSubsystem defenciveSubsystem;

	private DockedShip(String name, PositiveInteger shieldHP, PositiveInteger hullHP, PositiveInteger powergridOutput,
			PositiveInteger capacitorAmount, PositiveInteger capacitorRechargeRate, PositiveInteger speed,
			PositiveInteger size) {
		this.name = name;
		this.shieldHP = shieldHP;
		this.hullHP = hullHP;
		this.powergridOutput = powergridOutput;
		this.capacitorAmount = capacitorAmount;
		this.capacitorRechargeRate = capacitorRechargeRate;
		this.speed = speed;
		this.size = size;
	}

	public static DockedShip construct(String name, PositiveInteger shieldHP, PositiveInteger hullHP,
			PositiveInteger powergridOutput, PositiveInteger capacitorAmount, PositiveInteger capacitorRechargeRate,
			PositiveInteger speed, PositiveInteger size) {
		return new DockedShip(name, shieldHP, hullHP, powergridOutput, capacitorAmount, capacitorRechargeRate, speed,
				size);
	}

	@Override
	public void fitAttackSubsystem(AttackSubsystem subsystem) throws InsufficientPowergridException {
		if (subsystem != null) {
			int totalNewPowerGridConsumption = subsystem.getPowerGridConsumption().value()
					+ getDefensiveSubsystemPowerGridConsumption().value();

			int powergridOutputValue = this.powergridOutput.value();

			if (totalNewPowerGridConsumption > powergridOutputValue) {
				throw new InsufficientPowergridException(totalNewPowerGridConsumption - powergridOutputValue);
			}
		}

		this.attackSubsystem = subsystem;
	}

	@Override
	public void fitDefensiveSubsystem(DefenciveSubsystem subsystem) throws InsufficientPowergridException {
		if (subsystem != null) {
			int totalNewPowerGridConsumption = subsystem.getPowerGridConsumption().value()
					+ getAttackSubsystemPowerGridConsumption().value();

			int powergridOutputValue = this.powergridOutput.value();

			if (totalNewPowerGridConsumption > powergridOutputValue) {
				throw new InsufficientPowergridException(totalNewPowerGridConsumption - powergridOutputValue);
			}
		}

		this.defenciveSubsystem = subsystem;

	}

	public PositiveInteger getAttackSubsystemPowerGridConsumption() {
		return this.attackSubsystem == null ? PositiveInteger.of(0) : this.attackSubsystem.getPowerGridConsumption();
	}

	public PositiveInteger getDefensiveSubsystemPowerGridConsumption() {
		return this.defenciveSubsystem == null ? PositiveInteger.of(0)
				: this.defenciveSubsystem.getPowerGridConsumption();
	}

	public CombatReadyShip undock() throws NotAllSubsystemsFitted {
		if (this.attackSubsystem == null) {
			if (this.defenciveSubsystem == null) {
				throw NotAllSubsystemsFitted.bothMissing();
			}
			else {
				throw NotAllSubsystemsFitted.attackMissing();
			}
		}
		else if (this.defenciveSubsystem == null) {
			throw NotAllSubsystemsFitted.defenciveMissing();
		}

		return new CombatReadyShip(this.name, this.shieldHP, this.hullHP, this.powergridOutput, this.capacitorAmount,
				this.capacitorRechargeRate, this.speed, this.size, this.attackSubsystem, this.defenciveSubsystem);
	}

}
